package jogo;

import java.util.Stack;

public class Baralho
{
    final private Peca[] pecas = new Peca[28];
    private Stack<Peca> baralho;
    private int qtdPecas;


    public Baralho()
    {
        qtdPecas = 0;
        criaPecas();
        baralho = new Stack<>();

    }

    private void criaPecas()
    {
        // se já criou, cai fora
        if (pecas[0] != null)
            return;

        int i = 0;
        for(int y=0; y<=6; y++)
            for(int x=0; x<=y; x++)
                pecas[i++] = new Peca(x, y);
    }

    public Peca retiraPeça()
    {
        if (baralho.isEmpty())
            return null;
        qtdPecas--;
        Peca p = baralho.pop();
        return (p);
    }


    private void swap(Peca[] p, int indice1, int indice2)
    {
        Peca tmp = p[indice1];
        p[indice1] = p[indice2];
        p[indice2] = tmp;
    }

    public void embaralha()
    {
        while(! baralho.isEmpty()) baralho.pop();

        Peca[] tmp = new Peca[pecas.length];
        System.arraycopy(pecas, 0, tmp, 0, pecas.length);

        final int maxPeca = pecas.length;
        int i1 = 0;
        int i2 = 0;
        int trocas = (pecas.length * 3) + (int) (Math.random() * (pecas.length * 6));
        for(int troca=0; troca<trocas; troca++)
        {
            i1 = 0 + (int) (Math.random() * maxPeca);
            i2 = 0 + (int) (Math.random() * maxPeca);
            swap(tmp, i1, i2);
        }

        for(int i=0; i<tmp.length; i++)
            baralho.push(tmp[i]);

        qtdPecas = tmp.length;
    }

    public int getQtdPecas()
    {
        return qtdPecas;
    }

    public boolean isEmpty()
    {
        return (baralho.isEmpty());
    }
}
