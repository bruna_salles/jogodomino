package jogo;

import actions.AbstractAction;
import actions.Action;
import actions.ActionJogarPeca;
import actions.ActionTypes;
import com.google.gson.Gson;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by bruna on 16/05/2017.
 */

public class ServidorTCP {
    public static final int NUMERO_DE_JOGADORES = 4;
    private static final int JOGADOR_UM = 0;
    private static final int JOGADOR_DOIS = 1;
    private static final int JOGADOR_TRES = 2;
    private static final int JOGADOR_QUATRO = 3;
    private int porta;
    public int pontoTimeA = 0;
    public int pontoTimeB = 0;

    public ServidorTCP(int porta) {
        this.porta = porta;
    }

    private Peca inputPeca(Jogador jogador, Jogo jogo) {

        try {

            while (true) {
                int porta = this.porta + jogador.Id - 1;
                System.out.print("Porta [" + porta + "]: abrindo porta... ");
                ServerSocket servidor = new ServerSocket(porta);
                // o método accept() bloqueia a execução até que o servidor receba um pedido de conexão
                System.out.println("ok");
                Socket cliente = servidor.accept();

                System.out.println("Porta [" + porta + "]: cliente conectado " + cliente.getInetAddress().getHostAddress() + ":" + porta);
                ObjectOutputStream saida = new ObjectOutputStream(cliente.getOutputStream());
                saida.flush();

                //qual e o proximo

                String dados = ActionTypes.getComandoProximoJogador(porta,
                        jogador.getPecas(),
                        jogo.getPecasJogadas(),
                        jogo.getPecaMesa(),
                        jogo.getPontosRodada(),pontoTimeA,pontoTimeB);

                System.out.print("Porta [" + porta + "]: enviando dados para o cliente (" + dados + ")... ");
                saida.writeObject(dados);
                saida.flush();
                System.out.print("ok\n");

                AbstractAction action = receberDados(cliente);

                cliente.close();
                servidor.close();

                if (action != null && action.getType().equals(ActionTypes.JOGAR_PECA)) {
                    ActionJogarPeca actionJogarPeca = (ActionJogarPeca) action;
                    return actionJogarPeca.getPayload().getPeca();
                }
            }
        } catch (Exception e) {
            System.out.println("Porta [" + porta + "]: erro " + e.getMessage());
        }

        return new Peca();
    }

    private AbstractAction receberDados(Socket cliente) {
        AbstractAction action = null;

        try {
            ObjectInputStream stream = new ObjectInputStream(cliente.getInputStream());
            String comando = (String) stream.readObject();

            Gson gson = new Gson();
            action = gson.fromJson(comando, Action.class);

            if (action.getType().equals(ActionTypes.JOGAR_PECA)) {
                action = gson.fromJson(comando, ActionJogarPeca.class);
            }

            stream.close();
            System.out.print("input recebido > " + comando + "\n");
        } catch (EOFException ex) {
            System.out.println("Porta [" + porta + "]: nao recebeu nada");
        } catch (IOException ex) {
            System.out.println("Porta [" + porta + "]: erro " + ex.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return action;
    }

    public void inicia() {

        ////-------------------------------------------------------------------------------------
        ////-------------------------------------------------------------------------------------
        ////-------------------------------------------------------------------------------------

        Teclado teclado = new Teclado();
        int minhaVez = 0;
        Jogo jogo = new Jogo(teclado);
        Baralho baralho = new Baralho();

        Jogador jogador1 = new Jogador('A', 01, 0, 0, new ArrayList<>());
        Jogador jogador2 = new Jogador('B', 02, 0, 0, new ArrayList<>());
        Jogador jogador3 = new Jogador('A', 03, 0, 0, new ArrayList<>());
        Jogador jogador4 = new Jogador('B', 04, 0, 0, new ArrayList<>());

        //distribuiPeçasIniciais
        //6 pedras para cada participante, sobram 4.
        /*Distribuir as pedras de forma aleatoria entre os
        jogadores. Deve ser um gerador de numeros aleatorios
        ''''agora''' no intervalo entre [1 e 28] para a selecao de cada
        uma das pedras. Lembrar que cada pedra deve ser
        alocada a apenas um jogador!*/
        while ((pontoTimeA <= 7) && (pontoTimeB <= 7)) {
        baralho.embaralha();

        for (int i = 0; i <= 5; i++) {
            jogador1.compra(baralho);
            jogador2.compra(baralho);
            jogador3.compra(baralho);
            jogador4.compra(baralho);
        }    //as 4 peças que sobraram estão em BARALHO


        //////////////////////////// VIEW ///////////////////////////////////

        System.out.println("/////////// JOGO DE DOMINÓ //////////\n\n\n\n");
        System.out.println("Primeira Jogada...\n\n");
        /*  Inicialmente quem tiver a maior carroca (mesmo valor
            nos dois lados da mesma peca), priorizando o "dozao"
            de sena (6 com 6) comeca. Se ninguem tiver sera o
            gabao (peca dobrada) de 5, 4, 3, etc.*/

        for (int i = 6; i >= 0; i--) {
            Peca p = new Peca(i, i);

            if (jogador1.verificarSeTemPeça(p)) { /* existe um dozao ou gabao*/
                System.out.println(" Vez do time >>   " + jogador1.time + " jogo.Jogador >> " + jogador1.Id);
                System.out.println("jogo.Peca Jogada >>>> " + p.toString());
                jogador1.joga(p,jogo);
                jogo.vez(p);
                minhaVez = JOGADOR_DOIS;
                break;
            }

            boolean tempeca2 = jogador2.verificarSeTemPeça(p);
            if (tempeca2) { /* existe um dozao ou gabao*/
                System.out.println(" Vez do time >>   " + jogador2.time + " jogo.Jogador >> " + jogador2.Id);
                System.out.println("jogo.Peca Jogada >>>> " + p.toString());
                jogador2.joga(p,jogo);
                jogo.vez(p);
                minhaVez = JOGADOR_TRES;
                break;
            }

            boolean tempeca3 = jogador3.verificarSeTemPeça(p);
            if (tempeca3) { /* existe um dozao ou gabao*/
                System.out.println(" Vez do time >>   " + jogador3.time + " jogo.Jogador >>" + jogador3.Id);
                System.out.println("jogo.Peca Jogada >>>> " + p.toString());
                jogador3.joga(p,jogo);
                jogo.vez(p);
                minhaVez = JOGADOR_QUATRO;
                break;
            }

            boolean tempeca4 = jogador4.verificarSeTemPeça(p);
            if (tempeca4) { /* existe um dozao ou gabao*/
                System.out.println(" Vez do time >>   " + jogador4.time + " jogo.Jogador >>  " + jogador4.Id);
                System.out.println("jogo.Peca Jogada >>>> " + p.toString());
                jogador4.joga(p,jogo);
                jogo.vez(p);
                minhaVez = JOGADOR_UM;
                break;
            }
        }


        /* PONTUAÇÃO E LOOP DO JOGO*/


            Peca pp = new Peca();
            boolean comprou = false;//verifica se jogador comprou por não ter peça
            int compradores = 0;
            while ((jogador1.qtdPecas > 0) || (jogador2.qtdPecas > 0) || (jogador3.qtdPecas > 0) || (jogador4.qtdPecas > 0) ||(compradores == 4)) {
                jogador1.pontoTotal = pontoTimeA;
                jogador2.pontoTotal = pontoTimeB;
                jogador3.pontoTotal = pontoTimeA;
                jogador4.pontoTotal = pontoTimeB;
                switch (minhaVez) {
                    case JOGADOR_UM:
                        System.out.println("\nVez Jogador 1");
                        // percorrer o arraylist e o jogador escolhe a peça
                        jogador1.mostraMao();
                        pp = inputPeca(jogador1, jogo);
                        if (pp.invalida()) {
                            compradores = compradores + 1;
                            if (baralho.isEmpty()) {
                                    comprou = false;//roda para direita e passa aa vez
                                    System.out.println("Jogador 1 sem peça. Próximo");
                                    minhaVez = ((minhaVez + 1) % NUMERO_DE_JOGADORES);
                                    break;
                            }
                            if (comprou) {
                                comprou = false;
                                System.out.println("Jogador 1 sem peça. Próximo");
                                minhaVez = ((minhaVez + 1) % NUMERO_DE_JOGADORES);
                                break;
                            }
                            if(!baralho.isEmpty())
                            jogador1.compra(baralho);
                            comprou = true;// se pulou a vez, compra
                            System.out.println("Jogador 1 comprou...");
                            jogador1.mostraMao();
                            continue;
                        }
                        compradores = 0;

                        jogador1.joga(pp,jogo);//chama método joga (jogo.Peca p);
                        jogo.vez(pp); //executa jogada = chamar classe jogo.Jogo (método "vez")
                        pontoTimeA = jogo.checaSeGanhou(jogador1); //checa se ganhou
                        if (jogo.rodaParaDireita(pp)) {
                            minhaVez = ((minhaVez + 1) % NUMERO_DE_JOGADORES);
                            break;
                        }//roda pra direita e passa a vez

                        else
                        if(!baralho.isEmpty())
                            jogador1.compra(baralho);//compra
                        //informo placar real time
                        break;

                    case JOGADOR_DOIS:
                        System.out.println("\nVez jogo.Jogador 2");
                        // percorrer o arraylist e o jogador escolhe a peça
                        jogador2.mostraMao();
                        pp = this.inputPeca(jogador2, jogo);
                        if (pp.invalida()) {
                            compradores = compradores + 1;
                            if (baralho.isEmpty()){
                                    comprou = false; //roda pra direita e passa a vez
                                    System.out.println("Jogador 2 sem peça. Próximo");
                                    System.out.println("jogo.Jogador 2 sem peça. Próximo");
                                    minhaVez = ((minhaVez + 1) % NUMERO_DE_JOGADORES);
                                    break;

                            }
                            if (comprou) {
                                comprou = false;
                                System.out.println("Jogador 2 sem peça. Próximo");
                                minhaVez = ((minhaVez + 1) % NUMERO_DE_JOGADORES);
                                break;
                            }
                            if(!baralho.isEmpty())
                            jogador2.compra(baralho);
                            comprou = true;// se pulou a vez, compra
                            System.out.println("Jogador 2 comprou...");
                            jogador1.mostraMao();
                            continue;
                        }
                        compradores=0;
                        jogador2.joga(pp,jogo);//chama método joga (jogo.Peca p);
                        jogo.vez(pp); //executa jogada = chamar classe jogo.Jogo (método "vez")
                        pontoTimeB = jogo.checaSeGanhou(jogador2); //checa se ganhou
                        if (jogo.rodaParaDireita(pp)) {
                            minhaVez = ((minhaVez + 1) % NUMERO_DE_JOGADORES);
                            break;
                        } //roda pra direita e passa a vezelse
                        else
                        if(!baralho.isEmpty())
                            jogador2.compra(baralho);//compra
                        //informo placar real time
                        break;

                    case JOGADOR_TRES:
                        System.out.println("\nVez Jogador 3");
                        // percorrer o arraylist e o jogador escolhe a peçajogador3.mostraMao();
                        pp = this.inputPeca(jogador3, jogo);
                        if (pp.invalida()) {
                            compradores = compradores + 1;
                            if (baralho.isEmpty()) {
                                    comprou = false; //roda pra direita e passa a vez
                                    System.out.println("Jogador 3 sem peça. Próximo");
                                    minhaVez = ((minhaVez + 1) % NUMERO_DE_JOGADORES);
                                    break;
                            }
                            if (comprou) {
                                comprou = false;
                                System.out.println("Jogador 3 sem peça. Próximo");
                                minhaVez = ((minhaVez + 1) % NUMERO_DE_JOGADORES);
                                break;
                            }
                            if(!baralho.isEmpty())
                            jogador3.compra(baralho);
                            comprou = true;// se pulou a vez, compra
                            System.out.println("Jogador 3 comprou...");
                            jogador3.mostraMao();
                            continue;
                        }
                        compradores=0;
                        jogador3.joga(pp,jogo);//chama método joga (Peça p);
                        jogo.vez(pp); //executa jogada = chamar classe jogo.Jogo (método "vez")
                        pontoTimeA = jogo.checaSeGanhou(jogador3); //checa se ganhou
                        if (jogo.rodaParaDireita(pp)) {
                            minhaVez = ((minhaVez + 1) % NUMERO_DE_JOGADORES);
                            break;
                        }//roda pra direita e passa a vez
                        else
                        if(!baralho.isEmpty())
                            jogador3.compra(baralho);//compra
                        //informo placar real time
                        break;

                    case JOGADOR_QUATRO:
                        System.out.println("\nVez jogo.Jogador 4");
                        // percorrer o arraylist e o jogador escolhe a peça
                        jogador4.mostraMao();
                        pp = this.inputPeca(jogador4, jogo);
                        if (pp.invalida()) {
                            compradores = compradores + 1;
                            if (baralho.isEmpty()) {
                                    comprou = false; //roda pra direita e passa a vez
                                    System.out.println("Jogador 4 sem peça. Próximo");
                                    minhaVez = ((minhaVez + 1) % NUMERO_DE_JOGADORES);
                                    break;
                            }
                            if (comprou) {
                                comprou = false;
                                System.out.println("Jogador 4 sem peça. Próximo");
                                minhaVez = ((minhaVez + 1) % NUMERO_DE_JOGADORES);
                                break;
                            }
                            if(!baralho.isEmpty())
                            jogador4.compra(baralho);
                            comprou = true;// se pulou a vez, compra
                            System.out.println("Jogador 4 comprou...");
                            jogador4.mostraMao();
                            continue;
                        }
                        compradores=0;
                        jogador4.joga(pp,jogo);//chama método joga (Peça p);jogo.vez(pp); //executa jogada = chamar classe jogo.Jogo (método "vez")
                        pontoTimeB = jogo.checaSeGanhou(jogador4); //checa se ganhou
                        if (jogo.rodaParaDireita(pp)) {
                            minhaVez = ((minhaVez + 1) % NUMERO_DE_JOGADORES);
                            break;
                        }//roda pra direita e passa a vez
                        else
                        if(!baralho.isEmpty())
                            jogador4.compra(baralho);//compra
                        //informo placar real time
                        break;


                }
            }
            System.out.println("Placar Time A = " + pontoTimeA + "\n");
            System.out.println("Placar Time B = " + pontoTimeB + "\n");//informar placar final de jogo
            jogo = new Jogo(teclado);
        }
    }
}





