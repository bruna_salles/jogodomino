package jogo;

import java.util.ArrayList;

public class Jogo{
    private Peca pecaMesa = new Peca();
    private ArrayList<Peca> pecasJogadas = new ArrayList<>();
    private Teclado teclado;
    private int pontosRodada;

    public int getPontosRodada() {
        return pontosRodada;
    }

    public Peca getPecaMesa() {
        return pecaMesa;
    }

    public ArrayList<Peca> getPecasJogadas() {
        return pecasJogadas;
    }

    public Jogo(Teclado teclado){
        this.pontosRodada=0;
        this.teclado = teclado;
    }

    public void vez (Peca minhaPeça){
        boolean validoDuasPontas = false;

        /*  Se a ultima pedra serviu em uma das pontas = 1 ponto
            Se a pedra for uma carroca = 2 pontos
            Se cada lado da pedra serviu para cada uma das
            pontas = 3 pontos
            Se for carroca e servir para as duas pontas = 4 pontos
            Ganha o jogo quem primeiro obtiver 7 pontos.*/

        int ladoEscolhidoDaJogada = minhaPeça.getLadoUm();

        if (pecaMesa.getLadoUm() == -1 || pecaMesa.getLadoDois() == -1){
            pecaMesa = minhaPeça;
        }

        if(minhaPeça.invertePeça().equals(pecaMesa)){ //se serve para as duas pontas
            validoDuasPontas = true;
        }

        if (minhaPeça.getLadoUm() == ladoEscolhidoDaJogada) {
            //if ((minhaPeça.ladoUm == pecaMesa.ladoUm && minhaPeça.ladoDois == pecaMesa.ladoDois) || (minhaPeça.ladoDois == pecaMesa.ladoUm && minhaPeça.ladoDois == pecaMesa.ladoDois))
            if (minhaPeça.equals(pecaMesa) || (minhaPeça.getLadoDois() == pecaMesa.getLadoUm() && minhaPeça.getLadoDois() == pecaMesa.getLadoDois()))
                pontosRodada = 3;

            else {
                pontosRodada = 1;
            }
            //Atualizou peça == jogo.Jogador Joga jogo.Peca
            //pecaMesa.ladoUm = minhaPeça.ladoDois; //a peça inserida encosta o lado compativel com a mesa e o outro lado se torna disponivel.
            if(minhaPeça.getLadoUm()== pecaMesa.getLadoUm() || minhaPeça.getLadoUm() == pecaMesa.getLadoDois()) {
                pecasJogadas.add(minhaPeça);
                if (minhaPeça.getLadoUm() == pecaMesa.getLadoUm()) {
                    Peca pp = new Peca(pecaMesa.getLadoUm(), pecaMesa.getLadoDois());
                    pecaMesa = new Peca(minhaPeça.getLadoDois(), pp.getLadoDois());
                }
                else
                if (minhaPeça.getLadoUm() == pecaMesa.getLadoDois()) {
                    Peca pp = new Peca(pecaMesa.getLadoUm(), pecaMesa.getLadoDois());
                    pecaMesa = new Peca(minhaPeça.getLadoDois(), pp.getLadoUm());
                }
            }
            else{
                if(minhaPeça.getLadoUm() == pecaMesa.getLadoDois()) {
                    System.out.print("Lado inválido...");
                    //vez(minhaPeça);
                }
            }
        }

        if  (minhaPeça.getLadoDois() == ladoEscolhidoDaJogada) {
            //if ((minhaPeça.ladoUm == pecaMesa.ladoUm && minhaPeça.ladoDois == pecaMesa.ladoDois) || (minhaPeça.ladoDois == pecaMesa.ladoUm && minhaPeça.ladoDois == pecaMesa.ladoDois))
            if (minhaPeça.equals(pecaMesa) || (minhaPeça.getLadoDois() == pecaMesa.getLadoUm() && minhaPeça.getLadoDois() == pecaMesa.getLadoDois())) {
                pontosRodada = 3;
            } else {
                pontosRodada = 1;
            }

            //Jogador também jogou Peça
            //peçaMesa.ladoDois = minhaPeça.ladoUm;
            if ((minhaPeça.getLadoDois() == pecaMesa.getLadoUm()) || (minhaPeça.getLadoDois() == pecaMesa.getLadoDois())) {
                pecasJogadas.add(minhaPeça);
                if (minhaPeça.getLadoDois() == pecaMesa.getLadoDois()) {
                    Peca pp = new Peca(pecaMesa.getLadoUm(), pecaMesa.getLadoDois());
                    pecaMesa = new Peca(pp.getLadoUm(), minhaPeça.getLadoUm());
                } else {
                    if (minhaPeça.getLadoDois() == pecaMesa.getLadoUm()) {
                        Peca pp = new Peca(pecaMesa.getLadoUm(), pecaMesa.getLadoDois());
                        pecaMesa = new Peca(pp.getLadoDois(), minhaPeça.getLadoUm());
                    }
                }
            }
            else {
                System.out.print("Lado inválido...");
                //vez(minhaPeça);
            }
        }

        if (minhaPeça.getLadoUm() == minhaPeça.getLadoDois()){ // é uma carroça
            pontosRodada= 2;
            if (validoDuasPontas){        // é carroça e vale para as duas pontas
                pontosRodada= 4;
            }
        }
        if(minhaPeça.getLadoUm()==minhaPeça.getLadoDois())
            pecasJogadas.remove(minhaPeça);

        imprimeMesa();
    }

    private void imprimeMesa() {
        System.out.printf("Pontos da Rodada: %d\n", pontosRodada); //informar os jogadores dos pontos da partida
        System.out.println("============================");
        System.out.print("Peças jogadas: ");
        for(Peca p : pecasJogadas){
            System.out.print(p.toString() + " ");

        }
        System.out.println("");
        System.out.println("Mesa: " + pecaMesa.toString());
        System.out.println("============================");
    }


    public int checaSeGanhou(Jogador jogador) {
        /*  Ganha a mao quem conseguir acabar primeiro com as
            pedras.Em caso de empate, ganha quem estiver com menos
            pontos, em caso de permanecer empatado, a rodada
            e cancelada.Ganha o jogo quem primeiro obtiver 7 pontos.*/
        //chamar método mostrar placar final
        if (jogador.qtdPecas == 0) {
            jogador.pontoTotal = pontosRodada + jogador.pontoTotal;
            return jogador.pontoTotal;
        }
        return 0;

        //jogador.pontoTotal = pontosRodada + jogador.pontoTotal;

    }


    public boolean rodaParaDireita(Peca peça) {
        boolean roda = false;
        /*  Rodando para a direita o proximo jogador coloca uma
            pedra que tenha o mesmo numero da pedra que esta
            em uma das pontas. Caso nao possua nenhuma
            pedra, o jogador passa a vez.*/

        //if (jogador.pecas.contains(peçaMesa.ladoUm) || jogador.pecas.contains(peçaMesa.ladoDois)){
        if(peça.verificaPeloMenosUmDosLadosIguais(pecaMesa)){
            roda = true;
        }

        return roda;
    }



}




