package jogo;

public class Peca {
    private int ladoUm; //NAO MEXER NISSO, NÃO CRIAR SETTER, OBJETO IMUTAVEL
    private int ladoDois; //NAO MEXER NISSO, NÃO CRIAR SETTER, OBJETO IMUTAVEL

    public Peca(int ladoUm, int ladoDois) {
        if(ladoUm >= 0 && ladoUm <= 6 && ladoDois >= 0 && ladoDois <= 6) {
            this.ladoUm = ladoUm;
            this.ladoDois = ladoDois;
        } else {
            throw new IllegalArgumentException("jogo.Peca inválida: ladoUm="+ladoUm + ", ladoDois="+ ladoDois +".");
        }
    }

    public Peca() {
        this.ladoUm = -1;
        this.ladoDois = -1;
    }

    public int getLadoUm() {
        return ladoUm;
    }

    public int getLadoDois() {
        return ladoDois;
    }

    public Peca invertePeça() {
        return new Peca(ladoDois, ladoUm);
    }

    public boolean invalida() {
        return ladoUm == -1 && ladoDois == -1;
    }

    public boolean verificaPeloMenosUmDosLadosIguais(Peca outraPeca){
        if((outraPeca.getLadoUm()== -1)&&(outraPeca.getLadoDois()== -1))
        {
            return true;
        }
        else
        if(this.ladoUm == outraPeca.ladoUm)
        {
                       return true;
        }
                 else
                     {
                 if(this.ladoDois == outraPeca.ladoDois)
                 {
                    return true;
                 }
                 else{
                 if(this.ladoUm == outraPeca.ladoDois)
                 {
                    return true;
                 }
                 else{
                 if(this.ladoDois == outraPeca.ladoUm)
                 {
                    return true;
                 }
                 else{
                 return false;
                 }
                     }
                 }
                     }
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Peca)) return false;
        Peca peca2 = (Peca) obj;
        return equals(peca2);
    }

    public boolean equals(Peca peca2) {
        if ((this.ladoUm == peca2.ladoUm) && (this.ladoDois == peca2.ladoDois))
            return true;
        return false;
    }

    @Override
    public String toString() {
        return "[" + ladoUm + "|" + ladoDois + "]";
    }
}








