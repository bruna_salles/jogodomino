package jogo;

import java.util.ArrayList;


public class Jogador
{
    public int qtdPecas;
    protected ArrayList <Peca> pecas;
    public char time; // time A e time B
    public int Id;
    public int pontoTotal;



    public Jogador(char time, int Id, int pontoT, int quant, ArrayList<Peca> pecas)
    {
        this.time = time;
        this.qtdPecas = quant;
        this.pecas = pecas;
        this.Id=Id;
        this.pontoTotal=pontoT;
    }

    public ArrayList<Peca> getPecas() {
        return pecas;
    }

    public boolean possuiPeçaIgual(Peca buscaPeca){
        for(Peca p : pecas){
            if(p.equals(buscaPeca)){
                return true;
            }
        }
        return false;
    }

    public boolean possuiPeçaDaMesa(Peca pecaDaMesa){
        for(Peca p : pecas){
            if(p.verificaPeloMenosUmDosLadosIguais(pecaDaMesa)){
                return true;
            }
        }
        return false;
    }

    public void retiraPeça(Peca p){
        for (int i = 0; i < pecas.size(); i++){
           if (pecas.get(i).equals(p)){
               pecas.remove(p);
               break;
           }
        }

    }

    public void mostraMao (){
        System.out.print("Peças Disponíveis: ");
        for(Peca pp : this.pecas){
            System.out.print(pp.toString() + "  ");
         }
        System.out.print("\n");
    }

    public boolean verificarSeTemPeça (Peca peça){
        for(Peca pp : this.pecas){
            if(pp.equals(peça)){
                return true;
            }
        }
        return false;
    }

    public Peca compra(Baralho baralho)
    {
        Peca peca = baralho.retiraPeça();
        if (peca != null)
        {
            pecas.add(peca);
            qtdPecas++;
        }
        return peca;
    }

    public void joga(Peca p,Jogo jogo)
    {
        if (p == null)
            return;
        if(verificarSeTemPeça(p))
        {
            if(p.verificaPeloMenosUmDosLadosIguais(jogo.getPecaMesa()))
            {
                pecas.remove(p);
                qtdPecas--;

                if (getQtdPecas() == 0){
                    System.out.println("Time " + this.time + "ganhou.");
                }
            }
        }
    }

    public int getQtdPecas()
    {
        return qtdPecas;
    }

}
