package jogo;

import java.util.Scanner;

/**
 * Created by bruna on 20/06/2017.
 */
public class Teclado {
    Scanner teclado = new Scanner(System.in);

    public Teclado() {

    }

    public int getLadoDaJogada(Peca p) {
        while(true){
            System.out.print("Entre com o lado da peça a ser jogado [a|b]: ");
            try {
                String input = teclado.nextLine();
                switch (input) {
                    case "a":
                        return p.getLadoUm();
                    case "b":
                        return p.getLadoDois();
                    default:
                        throw new Exception();
                }
            } catch (Exception ex) {
                System.out.println("Input inválido.");
            }
        }
    }

    public Peca inputPeça() {
        System.out.println("Entre com a peça...");

        int ladoUm = -1;
        int ladoDois = -1;

        int k = 0;
        while(k < 2) {
            System.out.print("Digite o lado " + (k == 0 ? "um" : "dois") + ": ");
            String input = teclado.nextLine();

            if(k == 0 && input.isEmpty()){
                //Neste caso o jogador passou a vez
                System.out.println("jogo.Jogador passa a vez...");
                //tem que comprar do baralho!!!!!!!!!!!!!!!! chamar método "compra" no servidor CASE minhavez
                return new Peca();
            }

            try {
                int inputInteger = Integer.parseInt(input);
                switch (k) {
                    case 0:
                        ladoUm = inputInteger;
                        break;
                    case 1:
                        ladoDois = inputInteger;
                        break;
                }
                k++;
            } catch(NumberFormatException ex) {
                System.out.println("Input inválido, digite novamente.");
            }
        }

        Peca pp = new Peca(ladoUm,  ladoDois);
        System.out.println("Nova peça: " + pp.toString() + "");

        return pp;
    }
}
