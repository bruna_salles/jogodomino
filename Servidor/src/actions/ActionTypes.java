package actions;

import com.google.gson.Gson;
import jogo.Peca;

import java.util.ArrayList;

public class ActionTypes {
	public static final String WAIT = "WAIT";
	public static final String PROXIMO_JOGADOR = "PROXIMO_JOGADOR";
	public static final String JOGAR_PECA = "JOGAR_PECA";

	private ActionTypes() {}

	public static String getComandoProximoJogador(int jogadorPorta,
												  ArrayList<Peca> pecas,
												  ArrayList<Peca> pecasJogadas,
												  Peca mesa,
												  int pontosDaRodada,int pontoTimeA
			                                      ,int pontoTimeB) {
		PayloadMaoDoJogador payload = new PayloadMaoDoJogador(jogadorPorta, pecas, pecasJogadas, mesa, pontosDaRodada, pontoTimeA, pontoTimeB);

		AbstractAction<PayloadMaoDoJogador> action = new ActionMaoDoJogador(PROXIMO_JOGADOR, payload);

		Gson gson = new Gson();
		return gson.toJson(action);
	}
}

