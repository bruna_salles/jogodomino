package actions;

import jogo.Peca;

public class PayloadJogarPeca {
	private int jogadorPorta;
	private Peca peca;

	PayloadJogarPeca(int jogadorPorta, Peca peca) {
		this.jogadorPorta = jogadorPorta;
		this.peca = peca;
	}

	public Peca getPeca() {
		return peca;
	}

	public int getJogadorPorta() {
		return jogadorPorta;
	}
}
