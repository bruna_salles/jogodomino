package core;

import jogo.ServidorTCP;

import java.io.IOException;
import java.net.InetAddress;

/**
 * Created by bruna on 02/07/2017.
 */
public class Bootstrap {

    public static void main(String args[]) throws IOException {

        byte[] b = InetAddress.getByName("localhost").getAddress();
        System.out.println(b[0] + "." + b[1] + "." + b[2] + "." + b[3]);
        System.out.println("Endereço: " + InetAddress.getByName("localhost").getHostAddress());
        // byte[] addr = {127, 0, 0, 1};
        //System.out.println(InetAddress.getByAddress(addr).getHostName());

        ServidorTCP servidor = new ServidorTCP(9000);

        servidor.inicia();
    }

}
