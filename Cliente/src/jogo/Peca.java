package jogo;

public class Peca {
    private int ladoUm; //NAO MEXER NISSO, NÃO CRIAR SETTER, OBJETO IMUTAVEL
    private int ladoDois; //NAO MEXER NISSO, NÃO CRIAR SETTER, OBJETO IMUTAVEL

    public Peca(int lado1, int ladoDois) {
        if(lado1 >= 0 && lado1 <= 6 && ladoDois >= 0 && ladoDois <= 6) {
            this.ladoUm = lado1;
            this.ladoDois = ladoDois;
        } else {
            throw new IllegalArgumentException("jogo.Peca invalida: ladoUm="+lado1 + ", ladoDois="+ ladoDois +".");
        }
    }

    public Peca() {
        this.ladoUm = -1;
        this.ladoDois = -1;
    }

    public int getLadoUm() {
        return ladoUm;
    }

    public int getLadoDois() {
        return ladoDois;
    }

    public Peca invertePeca() {
        return new Peca(ladoDois, ladoUm);
    }

    public boolean invalida() {
        return ladoUm == -1 && ladoDois == -1;
    }

    public boolean verificaPeloMenosUmDosLadosIguais(Peca outraPeca){
        return this.ladoUm == outraPeca.ladoUm || this.ladoDois == outraPeca.ladoDois ||
                this.ladoUm == outraPeca.ladoDois || this.ladoDois == outraPeca.ladoUm;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Peca)) return false;
        Peca peca2 = (Peca) obj;
        return equals(peca2);
    }

    public boolean equals(Peca peca2) {
        if ((this.ladoUm == peca2.ladoUm) && (this.ladoDois == peca2.ladoDois))
            return true;
        return false;
    }

    @Override
    public String toString() {
        return "[" + ladoUm + "|" + ladoDois + "]";
    }
}
