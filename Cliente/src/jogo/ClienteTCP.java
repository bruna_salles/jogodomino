package jogo;

import actions.ActionMaoDoJogador;
import actions.ActionTypes;
import actions.Action;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;


/**
 * Created by bruna on 21/06/2017.
 */
public class ClienteTCP implements Runnable {
    public static final int PORTA_PADRAO = 9000;
    private final int porta;
    private int tentativaDeConexao = 0;
    private boolean conectado = false;
    
    public String getJogadorNome() {
    	return "Jogador " + porta;
    }
    
    public ClienteTCP(int porta){   //construtor da porta (Cliente)
        this.porta = porta;
    }

    public void jogarPeca(String msg) {
    	Teclado teclado = new Teclado();
        Peca pp = teclado.inputPeca(msg);

        trocaMensagem(ActionTypes.getComandoJogarPeca(porta, pp));
    }
    
    public String trocaMensagem(String enviarComando) {
        String comandoRecebido = null;
        Socket cliente = null;
        try {
            cliente = getCliente();

            ObjectInputStream entrada = new ObjectInputStream(cliente.getInputStream());
            comandoRecebido = (String)entrada.readObject();
            System.out.println("Porta [" + porta + "]: recebeu comando: " + comandoRecebido);
            // entrada.close();
            //System.out.println("Porta [" + porta + "]: conexão encerrada.");

            ObjectOutputStream saida = new ObjectOutputStream(cliente.getOutputStream());
            saida.flush();
            saida.writeObject(enviarComando);
            System.out.println("Porta [" + porta + "]: enviou comando: " + enviarComando);
            saida.flush();
            cliente.close();
        } catch (IOException ex) {
            System.out.println("Porta [" + porta + "]: erro ao enviar comando. " + ex.getMessage());
            if(cliente != null) try {
                cliente.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(1000);
                trocaMensagem(enviarComando);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return comandoRecebido;
    }

    public Socket getCliente() {
        Socket cliente = null;
        while(cliente == null) {
            try {
                this.tentativaDeConexao++;
            	cliente = new Socket("localhost", porta);
            	conectado = true;
            } catch (IOException e) {
                try {
                	Thread.sleep(1000);
                } catch(InterruptedException ex) {
                	//ignore
                }
                return getCliente();
            }
        }
        return cliente;
    }
    
    public void esperarProximaVez() {
    	try {
    		Thread.sleep(1000);
    	} catch(InterruptedException ex) {
    		//ignore
    	}
    }
    
    public void jogar() {
    	while(true) {

            Action initAction = new Action(ActionTypes.WAIT, "");

	    	String comando = trocaMensagem(new Gson().toJson(initAction));
            ActionMaoDoJogador action = new Gson().fromJson(comando, ActionMaoDoJogador.class);

	        if(action.getPayload().getJogadorPorta() == this.porta) {
                String msg = "Jogador: " + (this.porta + 1 - PORTA_PADRAO) + "\n";
                msg += "Peças disponíveis: ";
                for (Peca p: action.getPayload().getPecas()) {
                    msg += p.toString() + " ";
                }
                msg += "\n";
                msg += "Peças jogadas: ";
                for(Peca p: action.getPayload().getPecasJogadas()){
                    msg += p.toString() + " ";
                }
                msg += "\n";
                msg += "Peça mesa: " + action.getPayload().getMesa();
                msg += "\n";
                msg += "Pontos da rodada: " + action.getPayload().getPontosDaRodada();
                msg += "\n";
                msg += "Pontos timeA: " + action.getPayload().getPontoTimeA();
                msg += "\n";
                msg += "Pontos timeB: " + action.getPayload().getPontoTimeB();
                jogarPeca(msg);
	        }
	        else {
	        	esperarProximaVez();
	        }
    	
    	}
    }

    @Override
    public void run() {
        try {
        	System.out.print("Porta [" + porta + "]: iniciando...");
            
            jogar();
            
        }
        catch(Exception e) {
            System.out.println("Erro: " + e.getMessage());

            run();

        }
    }
}
