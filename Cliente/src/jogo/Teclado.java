package jogo;

import javax.swing.*;
import java.util.Scanner;

/**
 * Created by bruna on 20/06/2017.
 */
public class Teclado {
    Scanner teclado = new Scanner(System.in);

    public Teclado() {

    }
    
    public String getInput(String msg) {
    	msg += "\nAção: Entre com a peça ex. 1 2";

        String ans = (String) JOptionPane.showInputDialog(null,
    	        msg,
                "JogoDomino",
                JOptionPane.INFORMATION_MESSAGE,
    	        null,
    	        null,
    	        null);
    	return ans;
    }

    public Peca inputPeca(String msg) {

        String input = getInput(msg);

        if(input == null || input.isEmpty()){
            //Neste caso o jogador passou a vez
            return new Peca();
        }
        
        int ladoUm = -1;
        int ladoDois = -1;
        try {
	        ladoUm = Integer.parseInt(input.split(" ")[0]);
	        ladoDois = Integer.parseInt(input.split(" ")[1]);
	    } catch(NumberFormatException ex) {
        	return inputPeca(msg);
        }
        
        
        Peca pp = new Peca(ladoUm,  ladoDois);

        return pp;
    }
}
