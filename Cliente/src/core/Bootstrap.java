package core;

import jogo.ClienteTCP;

public class Bootstrap {
    private static final int NUMERO_DE_JOGADORES = 4;

    public static void main(String[] args) throws InterruptedException {

    	iniciaClientes(NUMERO_DE_JOGADORES);

    }

    public static ClienteTCP[] iniciaClientes(int numeroDeJogadores){
        ClienteTCP[] clientes = new ClienteTCP[numeroDeJogadores];
        for(int i = 0; i < numeroDeJogadores; i++){
            clientes[i] = new ClienteTCP(9000+i);
            Thread thread = new Thread(clientes[i]);
            thread.start();
        }
        return clientes;
    }


}
