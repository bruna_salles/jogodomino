package actions;

import jogo.Peca;

import java.util.ArrayList;

public class PayloadMaoDoJogador {
	private int jogadorPorta;
	private ArrayList<Peca> pecas;
	private ArrayList<Peca> pecasJogadas;
	private Peca mesa;
	private int pontosDaRodada;
	private int pontoTimeA;
	private int pontoTimeB;

	public PayloadMaoDoJogador(int jogadorPorta, ArrayList<Peca> pecas, ArrayList<Peca> pecasJogadas,
                               Peca mesa, int pontosDaRodada, int pontoTimeA,int pontoTimeB ) {
		this.jogadorPorta = jogadorPorta;
		this.pecas = pecas;
		this.pecasJogadas = pecasJogadas;
		this.mesa = mesa;
		this.pontosDaRodada = pontosDaRodada;
		this.pontoTimeA = pontoTimeA;
		this.pontoTimeB = pontoTimeB;
	}

	public ArrayList<Peca> getPecas() {
		return pecas;
	}

	public int getJogadorPorta() {
		return jogadorPorta;
	}

	public ArrayList<Peca> getPecasJogadas() {
		return pecasJogadas;
	}

	public Peca getMesa() {
		return mesa;
	}

	public int getPontosDaRodada() {
		return pontosDaRodada;
	}

	public int getPontoTimeA() {return pontoTimeA;}

	public int getPontoTimeB() {return pontoTimeB;}
}