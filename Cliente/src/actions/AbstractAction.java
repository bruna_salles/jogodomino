package actions;

public abstract class AbstractAction<T> {
	private String type;
	private T payload;

	public AbstractAction(String type, T payload) {
		this.type = type;
		this.payload = payload;
	}

	public T getPayload() {
		return payload;
	}

	public String getType() {
		return type;
	}
}
