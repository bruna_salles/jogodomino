package actions;

import com.google.gson.Gson;
import jogo.Peca;

public final class ActionTypes {
	public static final String WAIT = "WAIT";
    public static final String PROXIMO_JOGADOR = "PROXIMO_JOGADOR";
    public static final String JOGAR_PECA = "JOGAR_PECA";

    private ActionTypes() {}

    public static String getComandoJogarPeca(int jogadorPorta, Peca peca) {

        PayloadJogarPeca payload = new PayloadJogarPeca(jogadorPorta, peca);
        ActionJogarPeca action = new ActionJogarPeca(JOGAR_PECA, payload);

        Gson gson = new Gson();
        return gson.toJson(action);
    }

}
